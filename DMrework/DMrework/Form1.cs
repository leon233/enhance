﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;

public struct BinData
{
    ulong DM_burnDataMemoryADD;
    ulong PMIC_burnDataMemoryADD;
    ulong GM_R_burnDataMemoryADD;
    ulong GM_G_burnDataMemoryADD;
    ulong GM_B_burnDataMemoryADD;
    int VcomData;
}

namespace DMrework
{
    public static class ImportDLL
    {
        [DllImport("DemuraCSV2BIN.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.None)]
        public static extern IntPtr GetDLLVersion();

        [DllImport("DemuraCSV2BIN.dll", CallingConvention = CallingConvention.Winapi, CharSet = CharSet.None)]
        public static extern int BinCreatFromMatAddressAndJS(string JsonString, string saveDir, string Plane_SN,BinData burnBinData);

        

    }
        public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //ImportDLL.CreateSimple();
            //
            string a = "";
            //ImportDLL.ReadVersion(0); ;
            string strpara ="";

            byte temp = 0xF3;
            SByte a1 = (sbyte)(temp);

            IntPtr pRet = ImportDLL.GetDLLVersion();
            string strRet = Marshal.PtrToStringAnsi(pRet);
            button1.Text = strRet;
            
            //BinCreatFromMatAddressAndJS()
        }
    }
}
