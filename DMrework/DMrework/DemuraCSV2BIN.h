﻿#pragma once

#if defined(DEMURACSV2BIN_EXPORTS)
#define DLLAPI_EXPORT __declspec(dllexport)
#else
#define DLLAPI_EXPORT __declspec(dllimport)
#endif
/*******************************************************
*支持demura 转bin的TCON 列表
********************************************************/
typedef enum {
	uNT_71102,
	uVD_Format,
	uNT_71267,
	uCSOT_FORMAT,
	uNT_71733B,
	uNT_71265,
	uCSOT_FORMAT_Dual
} uTCONtype;
/*******************************************************
*转bin过程所需的参数信息
int PlaneNum;demura 补偿的灰阶数量
int channel;补偿的通道数，mono demura：1，color demura：3 
int H_total;demura LUT 列数
int V_total;demura LUT 行数
int imageH;产品水平分辨率，UD：3840
int imageV;产品垂直分辨率，UD：2160
char BlockSizeH; //0:4x4  1:8x8   2:16x16
char BlockSizeV; //0:4x4  1:8x8   2:16x16
int LowBound;//Low limit:通常设定12@10bit
int HighBound;//High limit:通常设定1020@10bit
int Plane[5];//demura 补偿的灰阶；

********************************************************/
typedef struct BinParam {
	int PlaneNum;
	int channel;
	int H_total;
	int V_total;
	int imageH;
	int imageV;
	char BlockSizeH; //0:4x4  1:8x8   2:16x16
	char BlockSizeV; //0:4x4  1:8x8   2:16x16
	int LowBound;
	int HighBound;
	int Plane[5];
	float* burnDataIn;    //< 指向CPU内存的图像数据区
	unsigned long long burnDataAddress;

    int VcomProcFlag;
    int DemuraProcFlag;
	int GmProcFlag;  //0: no GMC,1:ACC VD 60Hz format,2:ACC VD 120Hz format,3:IQC
    int VcomAddress;
    int DemuraAddress;
    int AccAddress;
    int IqcAddress;
    int DefaultVcom;
    int GMInputBits;
    int GMOutputBits;
    int BinSize;
    int TconType;
    char* PMicCodeDir;

	char* BinName;
	char* CellCode;
	char* ModelCode;
	char* Revision;
	char* Week;
} BinParam;
/*******************************************************
待转bin的数据
float* burnDataIn; < 指向CPU内存的补偿数据区
unsigned long long DM_burnDataMemoryADD; <demura补偿数据的首地址，与* burnDataIn是两种获取补偿数据的方式，2选1
unsigned long long PMIC_burnDataMemoryADD; < PMIC数据首地址
unsigned long long GM_R_burnDataMemoryADD; < ACC/IQC/DGC R通道补偿数据首地址
unsigned long long GM_G_burnDataMemoryADD; < ACC/IQC/DGC G通道补偿数据首地址
unsigned long long GM_B_burnDataMemoryADD; < ACC/IQC/DGC B通道补偿数据首地址
char* PMIC_CodeDir; <PMIC default code路径

********************************************************/
typedef struct BinData {
  
  unsigned long long DM_burnDataMemoryADD;
  unsigned long long PMIC_burnDataMemoryADD;
  unsigned long long GM_R_burnDataMemoryADD;
  unsigned long long GM_G_burnDataMemoryADD;
  unsigned long long GM_B_burnDataMemoryADD;
  int VcomData;
} BinData;

class TCONabstract;

extern "C"  //__stdcallʹ��C/C++�������ܹ�����API
{
	/*******************************************************
	*CSV to bin file
	********************************************************/
	DLLAPI_EXPORT int __stdcall BinCreatApi(
		uTCONtype tconType,
		BinParam binParam,
		const char* saveDir);
	DLLAPI_EXPORT int __stdcall BinCreatFromMat(
		uTCONtype tconType,
		BinParam binParam,
		const char* saveDir);
	DLLAPI_EXPORT int __stdcall BinCreatFromMatAddress(
		uTCONtype tconType,
		BinParam binParam,
		const char* saveDir);
	/*********************************************************
		const char* JsonSring,包含转bin 过程所需的参数信息 
        const char* saveDir,制定生成的bin文件保存路径
		const char* Plane_SN,当前Plane/OC的序号
        BinData burnBinData，待转bin 的补正数据
	**********************************************************/
    DLLAPI_EXPORT int __stdcall BinCreatFromMatAddressAndJS(
        const char* JsonString,
        const char* saveDir,
		const char* Plane_SN,
        BinData burnBinData);
	DLLAPI_EXPORT char*  __stdcall GetDLLVersion();
	
}
int ReadBinParamFromJson(const char* JsonSring, BinParam& binParam);

